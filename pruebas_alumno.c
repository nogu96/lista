#include "lista.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h> 

#define VECTOR_SIZE 6

void** crear_valores() {
	void** arr = malloc(sizeof(void*) * VECTOR_SIZE);
	if(arr == NULL)
		return NULL;
	for(int i=0; i< VECTOR_SIZE; i++) {
		arr[i] = malloc(sizeof(void*));
		if(arr[i] == NULL) {
			free(arr);
			return NULL;
		}
	}

	return arr;
}

void insertar_primero() {
	printf("PRUEBAS INSERTAR PRIMERO\n");
	lista_t* lista = lista_crear();
	void** valores = crear_valores();

	for (int i=0; i<VECTOR_SIZE; i++) {
		lista_insertar_primero(lista, valores[i]);
		printf("Valor insertado: %p\n", valores[i]);
		printf("Nuevo primero: %p\n", lista_ver_primero(lista));
	}

	print_test("Lista no esta vacia", !lista_esta_vacia(lista));
	print_test("Tamaño de lista = 6", lista_largo(lista) == 6);
	print_test("Ultimo valor en la lista es el primero insertado", lista_ver_ultimo(lista) == valores[0]);

	lista_destruir(lista, free);
	free(valores);
}

void insertar_ultimo() {
	printf("PRUEBAS INSERTAR ULTIMO\n");
	lista_t* lista = lista_crear();
	void** valores = crear_valores();

	for (int i=0; i<VECTOR_SIZE; i++) {
		lista_insertar_ultimo(lista, valores[i]);
		printf("Valor insertado: %p\n", valores[i]);
		printf("Nuevo ultimo: %p\n", lista_ver_ultimo(lista));
	}

	print_test("Lista no esta vacia", !lista_esta_vacia(lista));
	print_test("Tamaño de lista = 6", lista_largo(lista) == 6);
	print_test("Ultimo valor en la lista es el ultimo insertado", lista_ver_ultimo(lista) == valores[VECTOR_SIZE-1]);

	lista_destruir(lista, free);
	free(valores);
}

void borrar_primero(lista_t* lista) {
	for(int i=0; i<VECTOR_SIZE; i++) {
		printf("Valor borrado: %p\n", lista_borrar_primero(lista));
	}
	print_test("Borrar vacio", lista_borrar_primero(lista) == NULL);
}

void pruebas_lista() {
	insertar_primero();
	insertar_ultimo();
}

//<!--------------------ITERADOR------------------------>

void iterador_insertar_ultimo() {
	printf("PRUEBA INSERTAR ULTIMO\n");
	lista_t* lista = lista_crear();
	lista_iter_t* iterador = lista_iter_crear(lista);
	void** valor = crear_valores();

	for(int i=0; i<VECTOR_SIZE; i++) {
		lista_iter_avanzar(iterador);
		lista_iter_insertar(iterador, valor[i]);
		printf("Valor insertado: %p\n", valor[i]);
		printf("Valor actual: %p\n", lista_iter_ver_actual(iterador));
	}

	print_test("Lista no esta vacia", !lista_esta_vacia(lista));

	lista_destruir(lista, free);
	lista_iter_destruir(iterador);
	free(valor);
}

void iterador_insertar() {
	printf("PRUEBA INSERTAR\n");
	lista_t* lista = lista_crear();
	lista_iter_t* iterador = lista_iter_crear(lista);
	void** valor = crear_valores();

	for(int i=0; i<VECTOR_SIZE; i++) {
		lista_iter_insertar(iterador, valor[i]);
		printf("Valor insertado: %p\n", valor[i]);
		printf("Valor actual: %p\n", lista_iter_ver_actual(iterador));
	}
	print_test("Lista no esta vacia", !lista_esta_vacia(lista));

	lista_destruir(lista, free);
	lista_iter_destruir(iterador);
	free(valor);
}

void iterador_borrar_desde_inicio() {
	printf("PRUEBA BORRAR DESDE INICIO\n");
	lista_t* lista = lista_crear();
	void** valor = crear_valores();

	for(int i=0; i<VECTOR_SIZE; i++) {
		lista_insertar_primero(lista, valor[i]);
		printf("Valor insertado al principio: %p\n", valor[i]);
	}
	printf("Ultimo en la lista: %p\n", lista_ver_ultimo(lista));

	lista_iter_t* iterador = lista_iter_crear(lista);

	for (int i = 0; i < VECTOR_SIZE; i++){
		printf("Ver ultimo: %p\n", lista_ver_ultimo(lista));
		printf("Valor borrado: %p\n", lista_iter_borrar(iterador));
		free(valor[i]);
	}

	print_test("Lista esta vacia", lista_esta_vacia(lista));
	lista_destruir(lista, free);
	lista_iter_destruir(iterador);
	free(valor);
}

void iterador_borrar_desde_final() {
	printf("PRUEBA BORRAR DESDE FINAL\n");
	lista_t* lista = lista_crear();
	void** valor = crear_valores();

	lista_iter_t* iterador = lista_iter_crear(lista);

	for(int i=0; i<VECTOR_SIZE-1; i++) {
		lista_iter_insertar(iterador, valor[i]);
		lista_iter_avanzar(iterador);
		printf("Valor insertado al final: %p\n", valor[i]);
	}
	int ultimoValor = VECTOR_SIZE-1;
	lista_iter_insertar(iterador, valor[ultimoValor]);
	printf("Valor insertado al final: %p\n", valor[ultimoValor]);


	for (int i = 0; i <= VECTOR_SIZE; i++){
		printf("Ver ultimo: %p\n", lista_ver_ultimo(lista));
		void* valor_ultimo = lista_iter_borrar(iterador);
		printf("Valor borrado: %p\n", valor_ultimo);
		free(valor_ultimo);
	}

	print_test("Lista no esta vacia", !lista_esta_vacia(lista));
	print_test("no se puede avanzar", !lista_iter_avanzar(iterador));

	lista_destruir(lista, free);
	lista_iter_destruir(iterador);
	free(valor);
}

void iterador_borrar_mitad_inicio() {
	printf("PRUEBA BORRAR MITAD\n");
	lista_t* lista = lista_crear();
	lista_iter_t* iterador = lista_iter_crear(lista);
	void** valor = crear_valores();

	for(int i=0; i<VECTOR_SIZE; i++) {
		lista_iter_insertar(iterador, valor[i]);
		printf("Valor insertado: %p\n", valor[i]);
	}

	printf("Valor actual: %p\n", lista_iter_ver_actual(iterador));

	for (int i = 0; i < VECTOR_SIZE/2; i++){
		void* valor_borrado = lista_iter_borrar(iterador);
		printf("Valor borrado: %p\n", valor_borrado);
		free(valor_borrado);
	}

	printf("Valor actual: %p\n", lista_iter_ver_actual(iterador));

	lista_destruir(lista, free);
	lista_iter_destruir(iterador);
	free(valor);
}

void iterador_borrar() {
	printf("PRUEBA BORRAR\n");
	lista_t* lista = lista_crear();
	lista_iter_t* iterador = lista_iter_crear(lista);
	void** valor = crear_valores();

	for(int i=0; i<VECTOR_SIZE; i++) {
		lista_iter_insertar(iterador, valor[i]);
		printf("Valor insertado: %p\n", valor[i]);
	}

	printf("Valor actual: %p\n", lista_iter_ver_actual(iterador));

	for (int i = 0; i < VECTOR_SIZE; i++){
		lista_iter_avanzar(iterador);
		void* valor_borrado = lista_iter_borrar(iterador);
		printf("Valor borrado: %p\n", valor_borrado);
		free(valor_borrado);
	}

	print_test("Lista no esta vacia", !lista_esta_vacia(lista));

	lista_destruir(lista, free);
	lista_iter_destruir(iterador);
	free(valor);
}

void iterador_avanzar() {
	printf("PRUEBA avanzar\n");
	lista_t* lista = lista_crear();
	void* valor = malloc(sizeof(void*));
	lista_iter_t* iter = lista_iter_crear(lista);

	lista_iter_insertar(iter, valor);
	printf("Valor insertado: %p\n", valor);
	lista_iter_avanzar(iter);

	print_test("Tamaño lista igual 1", lista_largo(lista) == 1);
	print_test("no se puede avanzar", lista_iter_avanzar(iter) == false);
	print_test("Valor actual es NULL", lista_iter_ver_actual(iter) == NULL);

	lista_destruir(lista, free);
	lista_iter_destruir(iter);
}

bool hacer_algo(void* valor, void* extra) {
	print_test("hace algo?",valor != extra);
	return true;
}

void lista_interna() {
	printf("PRUEBA LISTA INTERNA\n");
	lista_t* lista = lista_crear();
	void** valor = crear_valores();
	for(int i=0; i<VECTOR_SIZE; i++) 
		lista_insertar_primero(lista, valor[i]);

	lista_iterar(lista, hacer_algo, NULL);
	lista_destruir(lista, free);
	free(valor);
}

void pruebas_lista_alumno() {
	//iterador_insertar();
	//iterador_insertar_ultimo();
	//iterador_borrar_mitad_inicio();
	//iterador_borrar();	
	//iterador_borrar_desde_inicio();
	//iterador_borrar_desde_final();
	//iterador_avanzar();
	//lista_interna();
	//pruebas_lista();
}