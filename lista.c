#include "lista.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h> 

typedef struct nodo {
	void* valor;
	struct nodo* proximo;
} nodo_t;

struct lista {
	size_t tamanio;
	nodo_t* primero;
	nodo_t* ultimo;
};

struct lista_iter {
	lista_t* lista;
	nodo_t* actual;
	nodo_t* anterior;
};


//----------------Lista------------------
//devuelve un nodo que almacena el valor asignado.
//post-condicion: devuelve un nodo con su proximo.
nodo_t* crear_nodo(nodo_t* proximo, void* valor) {
	nodo_t* nodo = malloc(sizeof(nodo_t));
	if(nodo == NULL)
		return NULL;

	nodo->proximo = proximo;
	nodo->valor = valor;

	return nodo;
}

//Creo una lista vacia para que utilice Barbara. Vacia porque no tiene ningun nodo y su tamanio es 0.
//post: devuelve un puntero a la lista.
lista_t* lista_crear() {
	lista_t* lista = malloc(sizeof(lista_t));
	if(lista == NULL) 
		return NULL;

	lista->primero = NULL;
	lista->ultimo = NULL;
	lista->tamanio = 0;

	return lista;
}

//Verifica que la lista esta vacia mediante el tamanio de esta o esta NULL.
//pre-condicion: la lista tuvo que ser creada.
//post-condicion: devuelve true si la lista esta vacia.
bool lista_esta_vacia(const lista_t *lista) {
	return lista == NULL || lista->primero == NULL || lista->tamanio == 0;
}

//Creo un nuevo nodo y lo asigno como primero. Ese nuevo primero tiene que apuntar
//al anterior primer nodo.
//pre-condicion: Una lista ya creada y un dato para que almacene el primero.
bool lista_insertar_primero(lista_t* lista, void* dato) {
	nodo_t* nuevo_nodo;
	if(lista_esta_vacia(lista))
		nuevo_nodo = crear_nodo(NULL, dato);
	else
		nuevo_nodo = crear_nodo(lista->primero, dato);

	if(nuevo_nodo == NULL)
		return false;
	
	lista->primero = nuevo_nodo;	

	if(lista->ultimo == NULL)
		lista->ultimo = lista->primero;
	
	lista->tamanio +=1;
	
	return true;
}

//Creo un nuevo nodo y lo asigno como ultimo. Ese nuevo ultimo va a apuntar a NULL pero
//el anterior ultimo va a puntar al nuevo ultimo. En caso que la lista este vacia, el valor se va a asignar a primero.
//pre-condicion: Una lista ya creada y un dato para que almacene el ultimo.
bool lista_insertar_ultimo(lista_t *lista, void *dato) {
	if(lista_esta_vacia(lista))
		return lista_insertar_primero(lista, dato);
	
	nodo_t* nuevo_nodo = crear_nodo(NULL, dato);
	if(nuevo_nodo == NULL)
		return false;
	
	lista->ultimo->proximo = nuevo_nodo;
	lista->ultimo = nuevo_nodo;
	
	lista->tamanio +=1;

	return true;
}

//Borro el primer nodo de la lista. En caso de no poderse, retorno NULL. El nodo quien apunto el primero 
//va a ser el nuevo primero.
//pre-condicion: la lista esta creada.
//post-condicion: Devuelvo el vlaor del primer nodo y vuelvo su proximo como primer nodo. La lista pierde un elemento.
void* lista_borrar_primero(lista_t *lista) {
	if(lista_esta_vacia(lista)) 
		return NULL;

	void* valor_aux = lista_ver_primero(lista);
	if(lista->primero->proximo == NULL) {
		free(lista->primero);
		lista->primero = NULL;
		lista->ultimo = NULL;
	} else {
		nodo_t* ex_primero_aux = lista->primero;
		lista->primero = lista->primero->proximo;
		free(ex_primero_aux);
	}
	
	lista->tamanio -=1;

	return valor_aux;
}

//Deuvelvo el valor del pimer nodo.
//pre-condicion: la lista esta creada.
//post-condicion: devuelvo el valor del primer nodo si es que tiene. En caso que no devuelve NULL.
void* lista_ver_primero(const lista_t *lista) {
	if(lista_esta_vacia(lista))
		return NULL;

	return lista->primero->valor;
}

//Deuvelvo el valor del ultimo nodo.
//pre-condicion: la lista esta creada.
//post-condicion: devuelvo el valor del ultimo nodo si es que tiene. En caso que no, devuelve NULL. 
//El nuevo ultimo valor pasa a ser el anteultimo valor.
void* lista_ver_ultimo(const lista_t* lista) {
	if(lista_esta_vacia(lista))
		return NULL;

	if(lista->ultimo == NULL)
		return NULL;

	return lista->ultimo->valor;
}

//Devuelvo el tamanio de la lista.
//pre-condicion: la lista fue creada.
//post-condicion: devuelve un numero representado el largo de la lista.
size_t lista_largo(const lista_t *lista) {
	if(lista_esta_vacia(lista))
		return 0;

	return lista->tamanio;
}

//Elimino cada uno de los nodos de la lista. Si destruir_dato no es nulo, lo llamo por cada uno de los nodos en la lista.
//pre-condicion: la lista fue creada.
//post-condicion: se detruyeron todos los nodos de la lista y la lista tambien.
void lista_destruir(lista_t *lista, void destruir_dato(void *)) {
	while(!lista_esta_vacia(lista)) {
		void* valor_aux = lista_borrar_primero(lista);
		if(destruir_dato != NULL)
			destruir_dato(valor_aux);
	}
	free(lista);
}

//--------------Iterador------------------
lista_iter_t* lista_iter_crear(lista_t* lista) {
	lista_iter_t* iter = malloc(sizeof(lista_iter_t));
	if(iter == NULL)
		return false;

	iter->lista = lista;
	iter->anterior = NULL;
	if(lista_esta_vacia(lista))
		iter->actual = NULL;
	else
		iter->actual = lista->primero;

	return iter;
}

bool lista_iter_avanzar(lista_iter_t* iter) {
	if(iter->actual == NULL)
		return false;

	iter->anterior = iter->actual;
	iter->actual = iter->actual->proximo;

	return true;
}

void* lista_iter_ver_actual(const lista_iter_t* iter) {
	if (lista_esta_vacia(iter->lista) || lista_iter_al_final(iter))
		return NULL;

	return iter->actual->valor;
}

bool lista_iter_al_final(const lista_iter_t* iter) {
	return iter->actual == NULL;
}

void lista_iter_destruir(lista_iter_t* iter) {
	free(iter);
}

bool lista_iter_insertar(lista_iter_t* iter, void* dato) {
	if(lista_esta_vacia(iter->lista) || iter->actual == iter->lista->primero) {
		bool aux = lista_insertar_primero(iter->lista, dato);
		if(aux){
			iter->actual = iter->lista->primero;
		}

		return aux;	
	}

	if(lista_iter_al_final(iter)) {
		bool aux = lista_insertar_ultimo(iter->lista, dato);
		if(aux){
			if(iter->actual != NULL)
				iter->anterior = iter->actual;
			iter->actual = iter->lista->ultimo;
		}
		return aux;
	}

	nodo_t* nuevo_actual = crear_nodo(iter->actual, dato);
	if(nuevo_actual == NULL)
		return false;

	if(iter->anterior != NULL)
		iter->anterior->proximo = nuevo_actual;
	else
		iter->lista->ultimo = nuevo_actual;

	iter->actual = nuevo_actual;
	iter->lista->tamanio += 1;
	return true;
}

void iterar_hasta_ultimo(lista_iter_t* iter) {
	iter->actual = iter->lista->primero;
	while(iter->actual != iter->lista->ultimo) {
		iter->anterior = iter->actual;
		iter->actual = iter->actual->proximo;
	}
}

void* lista_iter_borrar(lista_iter_t* iter) {
	if(lista_esta_vacia(iter->lista) || lista_iter_al_final(iter))
		return NULL;

	void* valor_aux;

	if(iter->actual == iter->lista->primero) {
		valor_aux = lista_borrar_primero(iter->lista);
		if(lista_esta_vacia(iter->lista))
			iter->actual = NULL;
		else
			iter->actual = iter->lista->primero;

		return valor_aux;
	}

	if(iter->actual == iter->lista->ultimo) {
		valor_aux = iter->lista->ultimo->valor;
		free(iter->lista->ultimo);
		iter->lista->ultimo = iter->anterior;
		iter->lista->ultimo->proximo = NULL;
		iter->actual = NULL;
		iter->lista->tamanio -= 1;

		return valor_aux;	
	}

	nodo_t* ex_actual = iter->actual;
	valor_aux = ex_actual->valor;
	
	iter->anterior->proximo = iter->actual->proximo;
	iter->actual = iter->actual->proximo;
	iter->lista->tamanio -= 1;

	free(ex_actual);

	return valor_aux;
}

void lista_iterar(lista_t* lista, bool visitar(void* dato, void* extra), void* extra) {
	nodo_t* nodo_aux = lista->primero;

	while(nodo_aux != NULL && visitar(nodo_aux->valor, extra))
		nodo_aux = nodo_aux->proximo;
}