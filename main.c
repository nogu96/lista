#include "lista.h"
#include "testing.h"
#include <stdio.h>

void pruebas_lista_alumno(void);

/* ******************************************************************
 *                        PROGRAMA PRINCIPAL
 * *****************************************************************/

int main(void) {
    /* Ejecuta todas las pruebas unitarias. */
    pruebas_lista_alumno();

    return failure_count() > 0;
}
